'use strict';

/**
 * @ngdoc function
 * @name jurassicSpaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jurassicSpaApp
 */
angular.module('jurassicSpaApp')
  .controller('MainCtrl', function ($scope) {
	$scope.user = {
			title: 'Developer',
			email: 'ipsum@lorem.com',
			firstName: '',
			lastName: '' ,
			company: 'Google' ,
			address: '1600 Amphitheatre Pkwy' ,
			city: 'Mountain View' ,
			state: 'CA' ,
			biography: 'Loves kittens, snowboarding, and can type at 130 WPM.\n\nAnd rumor has it she bouldered up Castle Craig!',
			postalCode : '94043'
		};
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
