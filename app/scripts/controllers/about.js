'use strict';

/**
 * @ngdoc function
 * @name jurassicSpaApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jurassicSpaApp
 */
angular.module('jurassicSpaApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
