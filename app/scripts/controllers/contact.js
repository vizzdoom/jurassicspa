'use strict';

/**
 * @ngdoc function
 * @name jurassicSpaApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the jurassicSpaApp
 */
angular.module('jurassicSpaApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
