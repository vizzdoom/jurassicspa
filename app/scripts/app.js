'use strict';

/**
 * @ngdoc overview
 * @name jurassicSpaApp
 * @description
 * # jurassicSpaApp
 *
 * Main module of the application.
 */
angular
  .module('jurassicSpaApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
	'ngMaterial'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
	  .when('/contact', {
	    templateUrl: 'views/contact.html',
		controller: 'ContactCtrl'
	  })
      .otherwise({
        redirectTo: '/'
      });
  });
